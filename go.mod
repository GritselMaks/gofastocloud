module gitlab.com/fastogt/gofastocloud

go 1.18

require (
	gitlab.com/fastogt/gofastocloud_base v1.7.9
	gitlab.com/fastogt/gofastogt v1.4.11
)

require golang.org/x/sys v0.1.0 // indirect

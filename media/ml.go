package media

type ImageBox struct {
	UID        int     `json:"unique_component_id"`
	CID        int     `json:"class_id"`
	OID        int64   `json:"object_id"`
	Confidence float32 `json:"confidence"`
	Left       int     `json:"left"`
	Top        int     `json:"top"`
	Width      int     `json:"width"`
	Height     int     `json:"height"`
}

type MlNotificationInfo struct {
	Id     StreamId   `json:"id"`
	Images []ImageBox `json:"images"`
}

package media

import "gitlab.com/fastogt/gofastogt"

type OnlineUsers struct {
	Daemon int `json:"daemon"`
	Http   int `json:"http"`
	Vods   int `json:"vods"`
	Cods   int `json:"cods"`
}

type ServiceStatisticInfo struct {
	Cpu           float64               `json:"cpu"`
	Gpu           float64               `json:"gpu"`
	LoadAverage   string                `json:"load_average"`
	MemoryTotal   int64                 `json:"memory_total"`
	MemoryFree    int64                 `json:"memory_free"`
	HddTotal      int64                 `json:"hdd_total"`
	HddFree       int64                 `json:"hdd_free"`
	BandwidthIn   int                   `json:"bandwidth_in"`
	BandwidthOut  int                   `json:"bandwidth_out"`
	Uptime        int64                 `json:"uptime"`
	Timestamp     gofastogt.UtcTimeMsec `json:"timestamp"`
	TotalBytesIn  int64                 `json:"total_bytes_in"`
	TotalBytesOut int64                 `json:"total_bytes_out"`
	OnlineUsers   OnlineUsers           `json:"online_users"`
}

type PingInfo struct {
	Timestamp gofastogt.UtcTimeMsec `json:"timestamp"`
}

type InputStreamStatisticInfo struct {
	Id             int    `json:"id"`
	LastUpdateTime int64  `json:"last_update_time"`
	PrevTotalBytes int64  `json:"prev_total_bytes"`
	TotalBytes     int64  `json:"total_bytes"`
	Bps            int32  `json:"bps"`
	Dbps           string `json:"dbps"`
}

type OutputStreamStatisticInfo struct {
	Id             int    `json:"id"`
	LastUpdateTime int64  `json:"last_update_time"`
	PrevTotalBytes int64  `json:"prev_total_bytes"`
	TotalBytes     int64  `json:"total_bytes"`
	Bps            int32  `json:"bps"`
	Dbps           string `json:"dbps"`
}
